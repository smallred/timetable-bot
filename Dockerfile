FROM debian:stable
RUN useradd -ms /bin/bash timetablebot
USER timetablebot
COPY ./bin/timetablebot /data/timetablebot
COPY ./timetable-bot /data
WORKDIR /data
ENTRYPOINT ["/data/timetablebot"]
