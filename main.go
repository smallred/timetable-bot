package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	ics "github.com/arran4/golang-ical"
	"github.com/getsentry/sentry-go"
	sentrygin "github.com/getsentry/sentry-go/gin"
	"github.com/gin-gonic/gin"
)

var (
	dsn           string // url for Sentry
	semesterStart string // the start date of the new semester. For example, if it start at 2022/09/05, then the value is 2022/09/04
	semesterNo    string // "1" refers to the first semester while "2" refers to the second semester
	holidayWeek   string // Only apply to the first semester, i.e., the national day.
)

func main() {
	dsn = os.Getenv("TIMETABLEDSN")
	semesterStart = os.Getenv("TIMETABLESTART")
	semesterNo = os.Getenv("SEMESTERNO")
	holidayWeek = os.Getenv("HOLIDAYWEEK")

	err := sentry.Init(sentry.ClientOptions{
		Dsn:              dsn,
		TracesSampleRate: 0.05,
	})
	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}

	r := gin.Default()
	gin.SetMode(gin.ReleaseMode)
	r.Use(Cors(), sentrygin.New(sentrygin.Options{}))
	r.LoadHTMLFiles("static/index.html")
	r.StaticFS("/static", http.Dir("./static"))
	r.GET("/", func(ctx *gin.Context) {
		ctx.HTML(http.StatusOK, "index.html", gin.H{
			"status": "ok",
		})
	})
	r.GET("/health", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"msg":      "ok",
			"hostname": hostname,
		})
	})
	r.GET("/status", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"Semester Start": semesterStart,
			"Semester No.":   semesterNo,
			"Holliday Week":  holidayWeek,
			"Date":           time.Now().String(),
		})
	})
	r.POST("/timetable", timetableUpload)
	r.Run(":9092")

}

// weekDay receives the slice of colspan and the index of the specific course. For example, the index of course A is 1 and the colspan is [1 2 2 1 1 1 1], then it will return 1.
func weekDay(numSlice []int64, colIndex int64) int64 {

	switch {
	case colIndex <= numSlice[0]:
		return 1
	case colIndex > numSlice[0] && colIndex <= numSlice[0]+numSlice[1]:
		return 2
	case colIndex > numSlice[0]+numSlice[1] && colIndex <= numSlice[0]+numSlice[1]+numSlice[2]:
		return 3
	case colIndex > numSlice[0]+numSlice[1]+numSlice[2] && colIndex <= numSlice[0]+numSlice[1]+numSlice[2]+numSlice[3]:
		return 4
	case colIndex > numSlice[0]+numSlice[1]+numSlice[2]+numSlice[3] && colIndex <= numSlice[0]+numSlice[1]+numSlice[2]+numSlice[3]+numSlice[4]:
		return 5
	case colIndex > numSlice[0]+numSlice[1]+numSlice[2]+numSlice[3]+numSlice[4] && colIndex <= numSlice[0]+numSlice[1]+numSlice[2]+numSlice[3]+numSlice[4]+numSlice[5]:
		return 6
	case colIndex > numSlice[0]+numSlice[1]+numSlice[2]+numSlice[3]+numSlice[4]+numSlice[5] && colIndex <= numSlice[0]+numSlice[1]+numSlice[2]+numSlice[3]+numSlice[4]+numSlice[5]+numSlice[6]:
		return 7
	default:
		return 0

	}

}

// weekParse receive the week string (e.g. week: 2, 4) and then convert it to []int64 which contains the seperate week (e.g. [2 4])
func weekParse(weekString string) []int64 {
	weekAll := make(map[int64]bool, 15)
	exp1 := regexp.MustCompile(`[1-9]+\.?[0-9]*`)
	exp2 := regexp.MustCompile(`[1-9]+[-][0-9]+`)
	match1 := exp1.FindAllString(weekString, -1)

	if len(match1) != 0 {
		for _, v := range match1 {
			num, _ := strconv.ParseInt(v, 10, 64)
			weekAll[num] = true
		}
	}

	match2 := exp2.FindAllString(weekString, -1)

	if len(match2) != 0 {
		for _, v := range match2 {
			matchAlone := exp1.FindAllString(v, -1) // 2-4 -> [2 4]
			if len(matchAlone) == 2 {
				start, _ := strconv.ParseInt(matchAlone[0], 10, 64)
				end, _ := strconv.ParseInt(matchAlone[1], 10, 64)
				for i := start; i <= end; i++ {
					weekAll[i] = true
				}
			}

		}
	}

	var weekAllSlice []int64
	for k := range weekAll {
		weekAllSlice = append(weekAllSlice, k)
	}
	return weekAllSlice
}

func timeParse(timeSlice []string, weekDay int64, week int64, semester string) (time.Time, time.Time) {
	holidayWeekInt, err := strconv.ParseInt(holidayWeek, 10, 64)
	if err != nil {
		panic(err)
	}
	loc, _ := time.LoadLocation("Asia/Shanghai")
	start := timeSlice[0]
	end := timeSlice[len(timeSlice)-1]

	start = semester + " " + start
	end = semester + " " + end

	startTime, _ := time.ParseInLocation("2006/01/02 15:04", start, loc)
	endTime, _ := time.ParseInLocation("2006/01/02 15:04", end, loc)

	if semesterNo == "1" {
		if week < holidayWeekInt {
			startTime = startTime.AddDate(0, 0, int(weekDay)+(int(week)-1)*7)
			endTime = endTime.AddDate(0, 0, int(weekDay)+(int(week)-1)*7).Add(30 * time.Minute)
		} else {
			startTime = startTime.AddDate(0, 0, int(weekDay)+(int(week))*7)
			endTime = endTime.AddDate(0, 0, int(weekDay)+(int(week))*7).Add(30 * time.Minute)
		}
	} else {
		startTime = startTime.AddDate(0, 0, int(weekDay)+(int(week-1))*7)
		endTime = endTime.AddDate(0, 0, int(weekDay)+(int(week-1))*7).Add(30 * time.Minute)
	}

	return startTime, endTime

}

func timetableUpload(c *gin.Context) {
	file, header, err := c.Request.FormFile("file-data")
	if err != nil {

		c.JSON(http.StatusBadRequest, gin.H{
			"success": false,
			"name":    header.Filename,
			"size":    header.Size,
		})

		if hub := sentrygin.GetHubFromContext(c); hub != nil {
			hub.WithScope(func(scope *sentry.Scope) {
				scope.SetExtra("failToUpload", "Something wrong with uploading")
				hub.CaptureMessage("There is something wrong with uploading")
			})
		}

		c.Abort()
	}

	fileNew, err := ioutil.ReadAll(file)
	if err != nil {
		if hub := sentrygin.GetHubFromContext(c); hub != nil {
			errinfo := map[string]interface{}{"failToLoad": "Fail to load the file",
				"attachment": file}
			hub.WithScope(func(scope *sentry.Scope) {
				scope.SetExtras(errinfo)
				hub.CaptureMessage("There is something wrong with loading files")
			})
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"error":   err.Error(),
			"message": "fail to load the file, please check and try again later",
		})
		c.Abort()
	}
	expHtml := regexp.MustCompile(`<title>Student Timetable</title>[\s\S]*</tbody></table>`)
	matchHtml := expHtml.FindString(string(fileNew))

	if matchHtml == "" {
		if hub := sentrygin.GetHubFromContext(c); hub != nil {
			errinfo := map[string]interface{}{"unwanted html": "user sent unwanted html",
				"attachment": string(fileNew)}
			hub.WithScope(func(scope *sentry.Scope) {
				scope.SetExtras(errinfo)
				hub.CaptureMessage("User sent wrong html that cannot be parsed")
			})
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "fail to parse the file, please check and try again later",
		})

		c.Abort()
	}

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(matchHtml))
	if err != nil {
		if hub := sentrygin.GetHubFromContext(c); hub != nil {
			errinfo := map[string]interface{}{"failToParseHtml": "goquery fail to parse the html"}
			hub.WithScope(func(scope *sentry.Scope) {
				scope.SetExtras(errinfo)
				hub.CaptureMessage("goquery fail to parse the html")
			})
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"message": "fail to parse the file with goquery, please check and try again later",
		})

		c.Abort()
	}

	courseTimeList := make(map[string][]string, 15)

	result := doc.Find("tbody>tr")
	result.Next().Each(func(i int, s *goquery.Selection) {
		time := s.Find(".coltitle").Text()
		s.Find(".gridcell>table>tbody>tr>.R1").Each(func(i int, s *goquery.Selection) {

			_, ok := courseTimeList[s.Text()]
			if !ok {
				courseTimeList[s.Text()] = make([]string, 0, 2)
				courseTimeList[s.Text()] = append(courseTimeList[s.Text()], time)
			} else {
				courseTimeList[s.Text()] = append(courseTimeList[s.Text()], time)
			}

		})

	})

	courseLocationList := make(map[string]string, 15)

	result = doc.Find("tbody>tr")
	result.Next().Each(func(i int, s *goquery.Selection) {

		s.Find(".gridcell>table>tbody").Each(func(i int, s *goquery.Selection) {

			courseName := s.Find("tr>.R1").Text()
			// courseLocation := s.Find("tr>.R4").Text()    aborted: due to the change of ebridge
			courseLocation := s.Find("tr>.R2").Eq(1).Text()

			_, ok := courseLocationList[courseName]
			if !ok {
				courseLocationList[courseName] = courseLocation
			}

		})

	})

	courseWeekList := make(map[string]string, 15)
	result = doc.Find("tbody>tr")
	result.Next().Each(func(i int, s *goquery.Selection) {

		s.Find(".gridcell>table>tbody").Each(func(i int, s *goquery.Selection) {

			courseName := s.Find("tr>.R1").Text()
			courseWeek := s.Find("tr>.R4Student").Text()

			_, ok := courseWeekList[courseName]
			if !ok {

				courseWeekList[courseName] = courseWeek
			}

		})

	})

	courseWeekDayList := make(map[string]int64, 15)
	result = doc.Find(".maintable")
	result.Find("tr").Has(".coltitle").EachWithBreak(func(i int, s *goquery.Selection) bool {
		a := 0
		s.Find("td[width]").Each(func(i int, s *goquery.Selection) {
			a++
			if s.Text() == "" {

			} else {
				_, ok := courseWeekDayList[s.Find(".R1").Text()]
				if !ok {
					courseWeekDayList[s.Find(".R1").Text()] = int64(a)
				}
			}
		})
		return true
	})

	var colSpan []int64
	result = doc.Find("tbody>.rowtitle")
	result.Each(func(i int, s *goquery.Selection) {
		s.Find("td[colspan]").Each(func(i int, s *goquery.Selection) {
			temp, _ := s.Attr("colspan")
			num, err := strconv.ParseInt(temp, 10, 64)
			if err != nil {
				colSpan = append(colSpan, 1)
			} else {
				colSpan = append(colSpan, num)
			}
		})

	})

	index := 0
	cal := ics.NewCalendar()
	cal.SetMethod(ics.MethodRequest)
	for k, v := range courseLocationList {
		for _, week := range weekParse(courseWeekList[k]) {
			index++
			startTime, endTime := timeParse(courseTimeList[k], weekDay(colSpan, courseWeekDayList[k]), week, semesterStart)
			event := cal.AddEvent(fmt.Sprint(index))
			event.SetCreatedTime(time.Now().Local())
			event.SetDtStampTime(time.Now().Local())
			event.SetModifiedAt(time.Now().Local())
			event.SetStartAt(startTime)
			event.SetEndAt(endTime)
			event.SetLocation(v)
			event.SetSummary(k)

		}
	}

	fileContentDisposition := "attachment;filename=\"" + "timetable.ics" + "\""
	c.Header("Content-Type", "application/ics")
	c.Header("Content-Disposition", fileContentDisposition)
	c.Data(http.StatusOK, "application/ics", []byte(cal.Serialize()))

}

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		origin := c.Request.Header.Get("Origin")
		if origin != "" {
			c.Header("Access-Control-Allow-Origin", c.GetHeader("Origin"))
			c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
			c.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
			c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Cache-Control, Content-Language, Content-Type")
			c.Header("Access-Control-Allow-Credentials", "true")
		}
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		c.Next()
	}
}
